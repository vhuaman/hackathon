package com.bbva.hackathon.ServicioEvaluacion;


import com.bbva.hackathon.datos.RepositorioRespuesta;
import com.bbva.hackathon.modelo.Evaluado;
import com.bbva.hackathon.modelo.Evaluador;
import com.bbva.hackathon.modelo.Pregunta;
import com.bbva.hackathon.modelo.Respuesta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ServicioRespuestaImpl implements ServicioRespuesta {
    private List<Respuesta> listaRespuestas = new ArrayList<Respuesta>();

    @Autowired
    private RepositorioRespuesta repositorioRespuesta;

    public ServicioRespuestaImpl() {
		/*
		Definición: Para este ejemplo vamos a tener:
			- 3 evaluadores que son Diana, Gise y Víctor
			- 5 evaluados que son Richard, Alvin, Jorge, Aníbal y Javier
			- 4 preguntas que son:
				a. ¿Se compromete con lo planificado?
				b. ¿Colabora en la definición de la arquitectura?
				c. ¿Resuelve las dudas que tiene el PO y los usuarios?
				d. ¿Se preocupa por la calidad y excelencia técnica?

		Total respuestas posibles: 3*4*5 = 60 respuestas
		*/
        ////////////////////////////////////////////
        List<Evaluador> listaEvaluadores = new ArrayList<Evaluador>();
        Evaluador e1 = new Evaluador("3001", "Victor M.", "Huaman F.", "Risk", "Staffer");
        Evaluador e2 = new Evaluador("3002", "Diana", "Solís E.", "Datahub", "Scrum Master");
        Evaluador e3 = new Evaluador("3003", "Gisella", "Enzian C.", "Cronos", "Developer");
        listaEvaluadores.add(e1);
        listaEvaluadores.add(e2);
        listaEvaluadores.add(e3);
		/*
		Hacer lo mismo para Evaluados y para preguntas
		*/
        List<Evaluado> listaEvaluados = new ArrayList<Evaluado>();
        Evaluado evo1 = new Evaluado("5001","Juan F.","Mallma A.", "Canvia","Front End");
        Evaluado evo2 = new Evaluado("5002","Maria A.","Ramtes S.", "Stefanini","Developer");
        Evaluado evo3 = new Evaluado("5003","Andrea","Perrigo r.", "Entelgy","Scrum master");
        Evaluado evo4 = new Evaluado("5004","Edwin","Rojas T.", "Tata","Developer");
        Evaluado evo5 = new Evaluado("5005","Richard C","Zanabria D.", "Entelgy","Arquitecto");
        listaEvaluados.add(evo1);
        listaEvaluados.add(evo2);
        listaEvaluados.add(evo3);
        listaEvaluados.add(evo4);
        listaEvaluados.add(evo5);

        List<Pregunta> listaPreguntas = new ArrayList<Pregunta>();
        Pregunta preg1 = new Pregunta("9001","¿Se compromete con lo planificado?");
        Pregunta preg2 = new Pregunta("9002","¿Colabora en la definición de la arquitectura?");
        Pregunta preg3 = new Pregunta("9003","¿Resuelve las dudas que tiene el PO y los usuarios?");
        Pregunta preg4 = new Pregunta("9004","¿Se preocupa por la calidad y excelencia técnica?");
        listaPreguntas.add(preg1);
        listaPreguntas.add(preg2);
        listaPreguntas.add(preg3);
        listaPreguntas.add(preg4);

        int cont = 1;
        for (int i=0; i<listaEvaluadores.size(); i++) { //3
            for (int j=0; j<listaEvaluados.size(); j++) { //5
                for (int k=0; k<listaPreguntas.size(); k++) { //4
                    Respuesta resp = new Respuesta();
                    //resp.setId("R00"+ StringU StringUtils.leftPad(cont, "0", 3));  // 001 -> 099 ===> R00051
                    resp.setId(""+ cont);
                    resp.setEvaluador(listaEvaluadores.get(i));
                    resp.setEvaluado(listaEvaluados.get(j));
                    resp.setPregunta(listaPreguntas.get(k));
                    resp.setValor("");
                    resp.setDescripcion("");
                    listaRespuestas.add(resp);

                    cont++;
                }
            }
        }

    }

    @Override
    public List<Respuesta> obtenerRespuestas() {
        return getListaRespuestas();
    }

    @Override
    public Respuesta guardarRespuesta(Respuesta respuesta) {
        return repositorioRespuesta.save(respuesta);
    }

    @Override
    public Respuesta obtenerRespuestaPorId(String id) {
        for(Respuesta r: getListaRespuestas()){
            if(r.getId().equals(id)) {
                 System.out.println("Estoy en el if");
                return r;
            } else {
                System.out.println("Estoy en el else");
            }
        }
        return null;
    }

    @Override
    public Respuesta actualizarRespuesta(Respuesta respuesta) {
        return null;
    }

    @Override
    public void borrarRespuestaPorId(String id) {

    }

    public static void main(String[] args){
        ServicioRespuestaImpl sr = new ServicioRespuestaImpl();
        System.out.println(sr.getListaRespuestas());
    }

    public List<Respuesta> getListaRespuestas() {
        return listaRespuestas;
    }

    public void setListaRespuestas(List<Respuesta> listaRespuestas) {
        this.listaRespuestas = listaRespuestas;
    }
}
