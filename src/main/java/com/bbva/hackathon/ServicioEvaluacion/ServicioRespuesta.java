package com.bbva.hackathon.ServicioEvaluacion;

import com.bbva.hackathon.modelo.Respuesta;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ServicioRespuesta {

    //GET /respuestas
    public List<Respuesta> obtenerRespuestas();

    //POST /respuestas
    public Respuesta guardarRespuesta(Respuesta respuesta);

    //GET /respuestas/{id}
    public Respuesta obtenerRespuestaPorId(String id);

    //PUT /respuesta/{id} RequestBody
    public Respuesta actualizarRespuesta(Respuesta respuesta);

    //DELETE /respuesta/{id}
    public void borrarRespuestaPorId(String id);


}
