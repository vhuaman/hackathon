package com.bbva.hackathon.datos;

import com.bbva.hackathon.modelo.Respuesta;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioRespuesta extends MongoRepository<Respuesta, String> {

}
