package com.bbva.hackathon.modelo;

public class Evaluador extends Persona{
    String nombreEquipo;
    String nombreRol;

    public Evaluador() {
    }

    public Evaluador(String id, String nombres, String apellidos, String nombreEquipo, String nombreRol) {
        super(id, nombres, apellidos);
        this.nombreEquipo = nombreEquipo;
        this.nombreRol = nombreRol;
    }

    public String getNombreEquipo() {
        return nombreEquipo;
    }

    public void setNombreEquipo(String nombreEquipo) {
        this.nombreEquipo = nombreEquipo;
    }

    public String getNombreRol() {
        return nombreRol;
    }

    public void setNombreRol(String nombreRol) {
        this.nombreRol = nombreRol;
    }
}
