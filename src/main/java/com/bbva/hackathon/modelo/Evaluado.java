package com.bbva.hackathon.modelo;

public class Evaluado extends Persona {
    String nombreFabrica;
    String nombreRol;

    public Evaluado() {
    }

    public Evaluado(String id, String nombres, String apellidos, String nombreFabrica, String nombreRol) {
        super(id, nombres, apellidos);
        this.nombreFabrica = nombreFabrica;
        this.nombreRol = nombreRol;
    }

    public String getNombreFabrica() {
        return nombreFabrica;
    }

    public void setNombreFabrica(String nombreFabrica) {
        this.nombreFabrica = nombreFabrica;
    }

    public String getNombreRol() {
        return nombreRol;
    }

    public void setNombreRol(String nombreRol) {
        this.nombreRol = nombreRol;
    }

    @Override
    public String toString() {
        return "Evaluado{" +
                "nombreFabrica='" + nombreFabrica + '\'' +
                ", nombreRol='" + nombreRol + '\'' +
                ", id='" + id + '\'' +
                ", nombres='" + nombres + '\'' +
                ", apellidos='" + apellidos + '\'' +
                '}';
    }
}
