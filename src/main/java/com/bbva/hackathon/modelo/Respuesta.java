package com.bbva.hackathon.modelo;

import org.springframework.context.annotation.Description;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
@Document(collection = "respuestas")
public class Respuesta {
    @Id
    String id;
    Evaluado evaluado;
    Evaluador evaluador;
    Pregunta pregunta;
    String valor;
    String descripcion;

    public Respuesta() {
    }


    public Respuesta(String id, Evaluado evaluado, Evaluador evaluador, Pregunta pregunta, String valor, String descripcion) {
        this.id = id;
        this.evaluado = evaluado;
        this.evaluador = evaluador;
        this.pregunta = pregunta;
        this.valor = valor;
        this.descripcion = descripcion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Evaluado getEvaluado() {
        return evaluado;
    }

    public void setEvaluado(Evaluado evaluado) {
        this.evaluado = evaluado;
    }

    public Evaluador getEvaluador() {
        return evaluador;
    }

    public void setEvaluador(Evaluador evaluador) {
        this.evaluador = evaluador;
    }

    public Pregunta getPregunta() {
        return pregunta;
    }

    public void setPregunta(Pregunta pregunta) {
        this.pregunta = pregunta;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Respuesta{" +
                "id='" + id + '\'' +
                ", evaluado=" + evaluado +
                ", evaluador=" + evaluador +
                ", pregunta=" + pregunta +
                ", valor='" + valor + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }
}
