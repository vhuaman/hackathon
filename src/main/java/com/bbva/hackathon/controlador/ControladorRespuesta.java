package com.bbva.hackathon.controlador;

import com.bbva.hackathon.ServicioEvaluacion.ServicioRespuesta;
import com.bbva.hackathon.modelo.Respuesta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("${api.version}/Respuesta")

public class ControladorRespuesta {
    @Autowired
    private ServicioRespuesta servicioRespuesta;

    @GetMapping("/")
    public ResponseEntity obtenerRespuestas() {
        return ResponseEntity.ok(this.servicioRespuesta.obtenerRespuestas());
    }

    @GetMapping("/{id}")
    public ResponseEntity obtenerRespuestaPorId(@PathVariable String id) {
        Respuesta r = this.servicioRespuesta.obtenerRespuestaPorId(id);
        if (r != null) {
            return ResponseEntity.ok(r);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);

    }

    @PatchMapping("/unica")
    public ResponseEntity actualizarRespuestaPorId(@RequestBody Respuesta respuesta) {
        Respuesta r = new Respuesta();
        if (validarValorRespuesta(respuesta.getValor())) {
            r = this.servicioRespuesta.obtenerRespuestaPorId(respuesta.getId());
            if (r == null) {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
            r.setValor(respuesta.getValor());
            r.setDescripcion(respuesta.getDescripcion());
            servicioRespuesta.actualizarRespuesta(r);
        } else {
            return new ResponseEntity<>("Por favor revisar la respuesta", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(r, HttpStatus.OK);

    }


    @PatchMapping("/masiva")
    public ResponseEntity actualizarRespuestas(@RequestBody List<Respuesta> listaRespuestas) {
        List<Respuesta> listaRespuestasModificadas = new ArrayList<Respuesta>();
        for (Respuesta respuesta : listaRespuestas) {
            final Respuesta r = this.servicioRespuesta.obtenerRespuestaPorId(respuesta.getId());
            if (r == null) {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
            r.setValor(respuesta.getValor());
            r.setDescripcion(respuesta.getDescripcion());
            servicioRespuesta.actualizarRespuesta(r);
            listaRespuestasModificadas.add(r);
        }

        return new ResponseEntity<>(listaRespuestasModificadas, HttpStatus.OK);
    }

    public Boolean validarValorRespuesta (String valor) {
        Boolean flag  = false;
        try {
            Integer nuevoValor= Integer.parseInt(valor);
            if ( nuevoValor>5 || nuevoValor<1){
                flag  = false;
            } else {
                flag  = true;
            }
        } catch (NumberFormatException nfe) {
            flag = false;
        }

        return flag;
    }

    @PostMapping
    public ResponseEntity guardarRespuestas() {

        for (Respuesta resp:servicioRespuesta.obtenerRespuestas()) {
            servicioRespuesta.guardarRespuesta(resp);
        }
        return new ResponseEntity<>("Respuestas Actualizadas", HttpStatus.OK);
    }


}
